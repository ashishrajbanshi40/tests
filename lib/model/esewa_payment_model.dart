class EsewaPaymentAsset {
  double? amount;
  String? productName;
  String? productID;
  String? callBackURL;

  EsewaPaymentAsset(
      {this.amount, this.productName, this.productID, this.callBackURL});
}