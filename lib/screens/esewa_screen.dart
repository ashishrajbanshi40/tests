import 'package:flutter/material.dart';
import 'package:test_latest/model/esewa_payment_model.dart';

import '../services/esewa_services.dart';

class EsewaScreen extends StatelessWidget {
  const EsewaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: InkWell(
          onTap: () {
            EsewaPayments.ePayment(
                asset: EsewaPaymentAsset(
              amount: 55.0,
              productName: 'product name',
              callBackURL: 'callbackurl',
              productID: '6369e7367e6ce8e80277dbee',
            ));
          },
          child: Container(
            alignment: Alignment.center,
            height: 100,
            width: 300,
            color: Colors.grey,
            child: const Text('esewa'),
          ),
        ),
      ),
    );
  }
}
