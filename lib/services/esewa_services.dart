import 'package:esewa_flutter_sdk/esewa_config.dart';
import 'package:esewa_flutter_sdk/esewa_flutter_sdk.dart';
import 'package:esewa_flutter_sdk/esewa_payment.dart';
import 'package:esewa_flutter_sdk/esewa_payment_success_result.dart';
import 'package:esewa_pnp/esewa_pnp.dart';
import 'package:flutter/material.dart';
import 'package:test_latest/end_point/endpoints.dart';

import '../model/esewa_payment_model.dart';

class EsewaPayments {
  static Future<ESewaResult?> ePayment(
      {required EsewaPaymentAsset asset}) async {

    EsewaFlutterSdk.initPayment(
        esewaConfig: EsewaConfig(
          environment: Environment.test,
          clientId: 'KBYJFkE3AgIQERIJVyMSEQQYF0kLAg0f',
          secretId: 'WBYWEhYSRSA6Kj4oMjMwLks0ID8kWSU6LCwtJD8=',
        ),
        esewaPayment: EsewaPayment(
          productId: "6369ebe67e6ce8e80277dd88",
          productName: "Test Product One",
          productPrice: "51",
          callbackUrl: verifyEsewaPaymentUrl,
        ),
        onPaymentSuccess: (EsewaPaymentSuccessResult data) {
          debugPrint(":::SUCCESS::: => $data");
        },
        onPaymentFailure: (data) {
          debugPrint(":::FAILURE::: => $data");
        },
        onPaymentCancellation: (data) {
          debugPrint(":::CANCELLATION::: => $data");
        });
  }
}
